//
//  Cell.swift
//  Notebook
//
//  Created by Tareq Saifullah on 25/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit
protocol Cell:class{
    /// A default reuse identifier for the cell class
       static var defaultReuseIdentifier: String { get }
}

extension Cell {
    static var defaultReuseIdentifier: String {
        // Should return the class's name, without namespacing or mangling.
        // This works as of Swift 3.1.1, but might be fragile.
        return "\(self)"
    }
}
