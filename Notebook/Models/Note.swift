//
//  Note.swift
//  Notebook
//
//  Created by Tareq Saifullah on 25/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation
class Note{

//    The Date Note was created
    let creationDate:Date
    
//    The note's text
    var text:String
    
    init(text:String = "New Note") {
        self.text = text
        creationDate = Date()
    }
    
    
}
