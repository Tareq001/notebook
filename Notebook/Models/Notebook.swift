//
//  Notebook.swift
//  Notebook
//
//  Created by Tareq Saifullah on 25/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation

class Notebook{
//    Name of Notebook
    let name:String

//    The date notebook was created
    let creationDate:Date
    
//    The notes contained by the notebook
    var notes:[Note] = []
    
    init(name:String) {
        self.name = name
        creationDate=Date()
        notes = []
    }
    
}

extension Notebook{
//    Add a note to the note book
    func addNote() {
        notes.append(Note())
    }
    
//    Removes the note at specific index
    func removeNote(at index:Int) {
        notes.remove(at: index)
    }
}
