//
//  NotebookCell.swift
//  Notebook
//
//  Created by Tareq Saifullah on 25/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

internal final class NotebookCell:UITableViewCell,Cell{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pageCountLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        pageCountLabel.text = nil
    }
}
