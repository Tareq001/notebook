//
//  NoteCell.swift
//  Notebook
//
//  Created by Tareq Saifullah on 25/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

internal final class NoteCell: UITableViewCell, Cell {
    // Outlets
    @IBOutlet weak var textPreviewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        textPreviewLabel.text = nil
        dateLabel.text = nil
    }
}
